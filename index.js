#!/usr/bin/node
var crypto = require("crypto"),
    argv = require("minimist")(process.argv.slice(2)),
    rsa = require("node-rsa"),
    fs = require("fs"),
    chalk = require("chalk"),
    path = require("path"),
    uuid = require("uuid/v4")

if (argv._[0] == "newkey" && argv._[1]) {
    console.log(chalk.bold.blue("∙∙∙") + chalk.reset(" Generating a new key pair. "))
    if (argv._[2]) {   
        if (argv._[2] % 8 != 0) {
            console.log(chalk.bold.red("∙∙∙") + chalk.reset(" Bitfield must be a multiple of 8. "))
            process.exit()
        }
    }
    let key = new rsa({b: argv._[2] || 2048})
    fs.writeFileSync(argv._[1], JSON.stringify({id: uuid(), pub: key.exportKey("public"), prv: key.exportKey("private")}))
    console.log(chalk.bold.blue("∙∙∙") + chalk.reset(" Generated key pair. "))
    process.exit() 
}

if (argv._[0] == "sign" && fs.existsSync(argv._[1]) && fs.existsSync(argv._[2])) {
    console.log(chalk.bold.blue("∙∙∙") + chalk.reset(" Signing file. "))
    var cr = new rsa(JSON.parse(fs.readFileSync(argv._[2]))["prv"].toString()).encryptPrivate(crypto.createHash('md5').update(fs.readFileSync(argv._[1]).toString()).digest("hex")).toString("hex"),
        id = JSON.parse(fs.readFileSync(argv._[2]))["id"].toString(),
        signage = (fs.existsSync(argv._[1] + ".sgn")) ? JSON.parse(fs.readFileSync(argv._[1] + ".sgn")) : {};
    signage[id] = cr
    fs.writeFileSync(argv._[1] + ".sgn", JSON.stringify(signage))
    console.log(chalk.bold.blue("∙∙∙") + chalk.reset(" Signed file. "))
    process.exit()
}

if (argv._[0] == "verify" && fs.existsSync(argv._[1]) && fs.existsSync(argv._[2])) {
    console.log(chalk.bold.blue("∙∙∙") + chalk.reset(" Verifying file. "))
    var cr = new rsa(JSON.parse(fs.readFileSync(argv._[2]))["prv"].toString()).encryptPrivate(crypto.createHash('md5').update(fs.readFileSync(argv._[1]).toString()).digest("hex")).toString("hex"),
        id = JSON.parse(fs.readFileSync(argv._[2]))["id"].toString(),
        signage = (fs.existsSync(argv._[1] + ".sgn")) ? JSON.parse(fs.readFileSync(argv._[1] + ".sgn")) : {};
    if (cr == signage[id]) {
        console.log(chalk.bold.blue("∙∙∙") + chalk.reset(" File was not tampered with. "))
    } else {
        console.log(chalk.bold.blue("∙∙∙") + chalk.reset(" File might not be in original condition. Proceed with caution. "))
    }
    process.exit()
}

console.log(`   Usage:
    newkey  [     output file     ] : Generate a new keypair that cryptodoc can read.
    sign    [input file] [key file] : Sign input file with specified key file.
    verify  [input file] [key file] : Verify file with .sgn against provided key file.`)
